Card = function Card(value, suit) { 
	this.value = value;
	this.suit = suit;
	this.numValue = getCardValue(value);
	this.numSuit = getCardSuitValue(suit);
	this.img = getCardImg(value, suit);
};

Card.prototype.getNumericValue = function() {
	return getCardValue(this.value);
};
Card.prototype.getSuitValue = function() {
	return getCardSuitValue(this.suit);
};
Card.prototype.getString = function() {
	return this.value+this.suit;
}

// Gets the numeric value.
var getCardImg = function getCardImg(v,s) {
		var valueString;
		var suitString;
		switch(s) {
			case "H":
				suitString = "/Hearts";
				break;
			case "D":
				suitString = "/Diamonds";
				break;
			case "C":
				suitString = "/Clubs";
				break;
			case "S":
				suitString = "/Spades";
				break;
			default:
				return undefined;
				break;
		}
		valueString = suitString+"_"+v+".png";
		return "/img"+suitString+valueString;
};

// Gets the numeric value.
var getCardValue = function getCardValue(v) {
	if(parseInt(v)) {
		return parseInt(v);
	} else {
		switch(v.toUpperCase()) {
			case "J":
				return 11;
				break;
			case "Q":
				return 12;
				break;
			case "K":
				return 13;
				break;
			case "A":
				return 14;
				break;
			default:
				return undefined;
				break;
		}
	}
};

// Gets numeric value of suit.
var getCardSuitValue = function getCardSuitValue(s) {
	switch(s) {
		case "H":
			return 1;
			break;
		case "D":
			return 2;
			break;
		case "C":
			return 3;
			break;
		case "S":
			return 4;
			break;
	}
}