//FileHandler singleton. Used by CardGenieIDS317.js. Uses FileReader, FileWriter, Globals.
fileHandler = {
	validate: function validateFile(file) {
		console.log("Validating file...");
		if(removeErrors()) {
			var errorExists = false;
			//TODO: Add function that returns if file's mime type belongs to the list of supported mime types.
			if(!fileSupported(file)) {
			 	console.log("FILE_TYPE_INVALID");
			 	Errors.update({_id: Errors.findOne({name: "FILE_TYPE_INVALID"})['_id']}, {$set:{found: true}});
			 	errorExists = true;
			}
			if(file.size >= MAX_FILE_SIZE) {
			 	console.log("FILE_SIZE_TOO_LARGE");
				Errors.update({_id: Errors.findOne({name: "FILE_SIZE_TOO_LARGE"})['_id']}, {$set:{found: true}});
				errorExists = true;
			} 
		 // 	for(var errorType in errorTypesEnum) {
			// 	if(errorTypesEnum[errorType].found) {
			// 		errorExists = true;
			// 	}
			// }
			if(!errorExists) {
				console.log("File is valid.");
				Session.set("errorExists", false);
				fileReader.read(file);
			} else { 
				Session.set("errorExists", true); 
				restartGame();
			}
		}
	}
};

var fileSupported = function fileSupported(file) {
	var value = false;
	for(var i=0; i < supportedMimeTypes.length; i++) {
		value = file.type.match(supportedMimeTypes[i]);
		if(value) { break; }
	}
	return value;
};