Hand = function Hand(first, second, third, fourth) { 
	this.first = first;
	this.second = second;
	this.third = third;
	this.fourth = fourth;
	this.cards = [this.first, this.second, this.third, this.fourth];
}

Hand.prototype.evaluate = function() {
	var h = this.cardsByValue(true);
	var v = undefined;
	var suitsMatch = false;
	if((h[0].suit == h[1].suit) && (h[1].suit == h[2].suit) && (h[2].suit == h[3].suit)) {
		v = HandValues.findOne({name:"Flush"});
		suitsMatch = true;
	}
	if((h[0].value == h[1].value) && (h[1].value == h[2].value) && (h[2].value == h[3].value)) {
		v = HandValues.findOne({name:"Full House"});
	}
	if((h[0].numValue == h[1].numValue+1) && (h[1].numValue == h[2].numValue+1)
									 && (h[2].numValue == h[3].numValue+1)) {
		v = suitsMatch ? HandValues.findOne({name:"Straight Flush"}) : HandValues.findOne({name:"Straight"});

	}
	if(v == undefined) { v = HandValues.findOne({name:"Nothing..."}); }
	return v;
};
Hand.prototype.cardsByValue = function(desc) {
	var c = this.cards;
	var r = [];
	for(var i=0; i<c.length;i++) {
		r.push(c[i]);
	}
	r = desc ? r.sort(function(a,b) {return b.numValue - a.numValue }) : r.sort(function(a,b) {return a.numValue - b.numValue });
	return r;
};
Hand.prototype.cardsBySuit = function(desc) {
	var c = this.cards;
	var r = [];
	for(var i=0; i<c.length;i++) {
		r.push(c[i]);
	}
	r = desc ? r.sort(function(a,b) {return b.numSuit - a.numSuit }) : r.sort(function(a,b) {return a.numSuit - b.numSuit });
	return r;
};
Hand.prototype.cardsByValueAndSuit = function() {
	var c = this.cards;
	var r = [];
	for(var i=0; i<c.length;i++) {
		r.push(c[i]);
	}
	return r.sort(function(a,b) {return ((a.numSuit*10) - a.numValue) - ((b.numSuit*10) - b.numValue) });
};