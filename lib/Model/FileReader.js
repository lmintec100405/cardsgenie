//FileReader singleton.
fileReader = {
	read: function readFile(file) {
			console.log("Reading file...");
			readFileAccordingly(file);
	},
	parseGame: function parseGame() {
		console.log("Debug (GAME_STRING Type): "+ Object.prototype.toString.call(GAME_STRING));
		if(Object.prototype.toString.call(GAME_STRING) === "[object Array]") {
			console.log("Preparing game...")
			if(Hands.length > 0) {
				restartGame();
			}
			for (var i=0; i< GAME_STRING.length; i++) {
				parseLine(GAME_STRING[i]);
			}
			Hands.length > 0 ? Session.set("currentTurn", 1) : console.log("No hands found in file.");

		} else { console.log("Failed to prepare game."); }
	}
};

//Splits game string in lines.
var splitGameString = function splitGameString() {
		GAME_STRING = GAME_STRING.split("\n");
			console.log("Split successful.");
			fileReader.parseGame();
	};

//Parses each line in GAME_STRING and adds the hands accordingly to the collection.
var parseLine = function parseLine (line) {
	var handString = line.trim().toUpperCase();
	handString = handString.replace(/\s/g, '');
	handString = handString.substr(0, 8);
	if(handString.length === 8) {
		var first = Cards.findOne({value: handString[0], suit: handString[1]});
		var second = Cards.findOne({value: handString[2], suit: handString[3]});
		var third = Cards.findOne({value: handString[4], suit: handString[5]});
		var fourth = Cards.findOne({value: handString[6], suit: handString[7]});
		var cardsArray = [first, second, third, fourth];
		if(handIsValid(cardsArray)) {
			Hands.push(new Hand(first, second, third, fourth));
			Session.set("hands", Hands);
			Session.set("currentHand", Session.get("hands")[Session.get("currentTurn")]-1);
			Session.set("maxTurns", Session.get("hands").length);
		} else {
			Session.set("invalidLines", Session.get("invalidLines")+1);
		}
	} else {
		Session.set("invalidLines", Session.get("invalidLines")+1);
	}
};

//Returns true if all the cards in the hand are valid.
var handIsValid = function validateHand (cardsArray) {
	var isValid = true;
	console.log(cardsArray);
	for(var i=0; i<cardsArray.length; i++) {
		var card = cardsArray[i];
		if(card == undefined) {
			isValid = false;
			break;
		}
	}
	return isValid;
}

var readFileAccordingly = function readFileAccordingly(file) {
			var reader = new FileReader();
			reader.onload = (function(theFile) {
				return function(e) {
				GAME_STRING = e.target.result;
				splitGameString();
				};
			})(file);
			try {
				reader.readAsText(file);
			} catch (e) {
				console.log(e);
			}
			switch(getFileFormat(file)) {
				case "/text.plain/":
					console.log("File type is: /text.plain/. No extra action (adapter) required.");
					break;
				case "/example.type/":
					console.log("File type is: /example.type/. Must format GAME_STRING using the appropriate filter/adapter.");
					break;
				default:
					break;
			}
};

var getFileFormat = function getFileFormat(file) {
	//get file Mime Type stub. Unneeded for now since it only accepts plain text files.
	return "/text.plain/";
};

