	GAME_STRING = "";
	MAX_FILE_SIZE = 104857600;


	Cards = new Meteor.Collection("cards");
	Errors = new Meteor.Collection("errors");
	HandValues = new Meteor.Collection("handvals");
	Hands = [];
	supportedMimeTypes = [/text.plain/];

//Error cleaner. Sets all error types' found property and errorsExist global variable to false for re-evaluation.
removeErrors = function removeErrors() {
	for(et in Errors) {
		Errors.update({_id: et['_id']}, {$set:{found:false}});
		// Errors.update({_id:{Errors.find({found: true}).fetch()}}, {$set:{found: false}}, {multi: true});
		 Session.set("errorsExist", false);
	}
	return true;
	 };

restartGame = function restartGame() {
	Hands = []
	Session.set("hands", Hands);
	Session.set("currentTurn", 0);
	Session.set("maxTurns", 0);
	invalidLines = 0;
}