if (Meteor.isClient) {

if (window.File && window.FileReader && window.FileList && window.Blob) {

  Template.errorBox.errorFound = function () {
    return Session.get("errorExists");
  };

  Template.errorBox.errorType = function() {
    var etArray = Errors.find({found: true}).fetch();
    return etArray;
  }

  Template.error.errorFound = function() {
    return this.found;
  }

  Template.turn.current = function() {
    return Session.get("currentTurn");
  }

  Template.turn.last = function() {
    return Session.get("maxTurns");
  }

  Template.hand.handIsNotEmpty = function () {
    var h = Session.get("hands") != undefined ? Session.get("hands") : Hands;
    return h.length > 0;
  }

  Template.hand.cards = function() {
    var st = Session.get("sortingType");
    var t = Session.get("currentTurn")-1;
    var isAsc = Session.get("ascDesc");
    var h = Session.get("hands").length != 0 ? Hands[t] : [];
    if(h.cards) {
      switch(st) {
        case 1:
            return h.cardsByValue(isAsc);
            break;
        case 2:
            return h.cardsBySuit(isAsc);
            break;
        case 3:
            return h.cardsByValueAndSuit();
            break;
        default:
            return h.cards;
            break;
      }
    }
    console.log(h);
    return h;
  }

  Template.handPoints.ptsAcc = function() {
    var t = Session.get("currentTurn") != undefined ? Session.get("currentTurn") : 0;
    var pts = 0;
    for (var i=0; i < t; i++) {
      pts+= Hands[i].evaluate().points;
    }
    Session.set("pointsAccumulated", pts);
    return Session.get("pointsAccumulated");
  }
  Template.handPoints.handName = function() {
    var t = Session.get("currentTurn") != undefined ? Session.get("currentTurn")-1 : 0;
    var c = Hands[t];
    return c != undefined ? c.evaluate().name : "Click New to start a new game. Open to resume a previous game.";
  }
  Template.handPoints.handPts = function() {
    var t = Session.get("currentTurn") != undefined ? Session.get("currentTurn")-1 : 0;
    var c = Hands[t];
    return c != undefined ? " (+"+c.evaluate().points+" pts)" : "";
  }
  Template.handPoints.finalScore = function() {
    var h = Hands ? Hands : [];
    var score = 0;
    for (var i = 0; i < h.length; i++) {
      score += h[i].evaluate().points;
    };
    return score;
  }

  Template.badLinesCounter.invalidLines = function() {
  return Session.get("invalidLines");
}

var handleFileChange = function(evt) {
    f = evt.target.files[0];
	  fileHandler.validate(f);
	  }
	 
var openButtonOnClick = function() {
  e1 = document.getElementById('fileinput');
  e1.click();
}

var nextButtonOnClick = function() {
  var max = Session.get("maxTurns");
  var current = Session.get("currentTurn");
  var slider = document.getElementById('turn-slider');
  var t;
  if(current < max) { t = current+1; }
  else { t = max; }
    slider.value = t.toString();
    Session.set("currentTurn", t);
    Session.set("currentHand", Hands[Session.get("currentTurn")-1]);
}

var keyListener = function(evt) {
  switch(evt.keyCode) {
    case 37:
      var previous = document.getElementById('previous-btn');
      previous.click();
      break;
    case 39:
      var next = document.getElementById('next-btn');
      next.click();
      break;
    default:
      break;
  }
}

var previousButtonOnClick = function() {
  var max = Session.get("maxTurns");
  var current = Session.get("currentTurn");
  var slider = document.getElementById('turn-slider');
  var t;
  if(current > 1) { t = current-1; }
  if(current > max) { t = max; }
  if(t) {
    slider.value = t.toString();
    Session.set("currentTurn", t);
    Session.set("currentHand", Hands[Session.get("currentTurn")-1]);
  }
}
var turnSliderOnChange = function() {
  var turnSlider = document.getElementById('turn-slider');
  var tv = parseInt(turnSlider.value);
  var max = Session.get("maxTurns");
  if(tv.value > max) { turnSlider.value = max.toString(); }
  Session.set("currentTurn", parseInt(turnSlider.value));
  Session.set("currentHand", Hands[Session.get("currentTurn")-1]);
}

var setSortingType = function(mask) {
  var st = Session.get("sortingType");
  var t;
  switch(mask.target.id) {
      case "value-sort":
        t = 1;
        console.log("Set sorting type to by value");
        break;
      case "suit-sort":
        t = 2;
        console.log("Set sorting type to by suit");
        break;
      case "both-sort":
        t = 3;
        console.log("Set sorting type to by both");
        break;
      default:
        t = 0;
        console.log("Set sorting type to none");
        break;
  }
  if(st == t) {
    Session.set("ascDesc", !Session.get("ascDesc"));
  } else {
    Session.set("sortingType", t);
    Session.set("ascDesc", true);
  }
}
	 
 Meteor.startup(function init () {
  Session.setDefault("hands", []);
  Session.set("hands", Hands);
  Session.set("pointsAccumulated", 0);
  Session.setDefault("pointsAccumulated", 0);
  Session.set("pointsFinal", 0);
  Session.setDefault("pointsFinal", 0);
  Session.set("errorExists", false);
  Session.setDefault("currentHand", []);
  Session.setDefault("currentTurn, 0");
  Session.setDefault("maxTurns, 0");
  Session.set("currentTurn", 0);
  Session.set("maxTurns", 0);
  Session.setDefault("sortingType", 0);
  Session.set("sortingType", 0);
  Session.setDefault("ascDesc", true);
  Session.set("ascDesc", true);
  Session.set("invalidLines", 0);
  var e1 = document.getElementById('fileinput');
  var openButton = document.getElementById('open-btn');
  var previous = document.getElementById('previous-btn');
  var next = document.getElementById('next-btn');
  var noSort = document.getElementById('no-sort');
  var valueSort = document.getElementById('value-sort');
  var suitSort = document.getElementById('suit-sort');
  var bothSort = document.getElementById('both-sort');
  var turnSlider = document.getElementById('turn-slider');
  noSort.onclick = setSortingType;
  valueSort.onclick = setSortingType;
  suitSort.onclick = setSortingType;
  bothSort.onclick = setSortingType;
  openButton.onclick = openButtonOnClick;
  previous.onclick = previousButtonOnClick;
  next.onclick = nextButtonOnClick;
  turnSlider.onchange = turnSliderOnChange;
  e1.addEventListener('change', handleFileChange, false);
  openButton.disabled = false;
  document.onkeydown = keyListener;
  });
} else
alert("File API not available.");
}


if (Meteor.isServer) {
    Meteor.startup(function() {
      console.log("Startup function running...");
      // Cards.remove({});
      // Errors.remove({});
      // HandValues.remove({});
      //Adds all card combinations to the collection. (Flyweights)
      // if(Cards.find().count() != 0) { Cards.remove({}); }
        if (Cards.find().count() === 0) {
          var cardNames = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"];
          var cardSuits = ["H", "D", "C", "S"];
          for (var i = 0; i < cardNames.length; i++) {
            for(var j = 0; j < cardSuits.length; j++) {
              Cards.insert(
                new Card(cardNames[i], cardSuits[j]));
            //   {
            //           number: {code: cardNames[i], value: i+2},
            //           suit: {code: cardSuits[j], value: j+1},
            //           getString: cardNames[i]+cardSuits[j] });
            // }
          }
        }
        console.log("Added all cards to collection.");
      }

      if(HandValues.find().count() === 0) {
        var handTypesArray = [
        {name: "Flush", description: 'All cards of the same suit. (all diamonds, etc.)', points: 10},
        {name: "Full House", description: 'All cards are of the same value. (all ases, etc.)', points: 100},
        {name: "Straight", description: 'Four consecutive cards. (4, 5, 6, and 7, and the like)', points: 300},
        {name: "Straight Flush", description: 'Four consecutive cards of the same suit. (4 of clubs, 5 of clubs, 6 of clubs, and 7 of clubs, and the like)', points: 500},
        {name: "Nothing...", description: 'Nothing special.', points: -10}
        ];

        for(var i=0; i < handTypesArray.length; i++) {
          HandValues.insert(handTypesArray[i]);
        }
      }


      //Creates collection of error types.
      // if(Errors.find().count != 0) { Errors.remove({}); }
      if(Errors.find().count() === 0) {
        //Array containing all types of errors.
        var errorTypesArray = [
          {name: "FILE_SIZE_TOO_LARGE",
            message_head: "File size too large", 
            message_description: "File size should not exceed 100 MB.", found: false },
          {name: "FILE_TYPE_INVALID",
            message_head: "Invalid file type", 
            message_description: "File should be a plain text file (.TXT).", found: false }
          ];
        for(var i=0; i < errorTypesArray.length; i++) {
          Errors.insert(errorTypesArray[i]);
        }  
      }
    });
}